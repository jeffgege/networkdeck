# V1.1
* Added site_tester lab
* Made configuration changes to Samba-Easy to make it solveable 

# V1.0
* Fixed issue where connection token volumes aren't being added because it was looking at the NETWORKDECK container path. Not the local computer path
* Updated setup.sh to include the new .env definition 
* Added prerequisite section to README
* Fixed logging issues where logs weren't being written in production mode

# V0.9
* Added shell installer to build docker images
* Changed auth.register page to be accessible only by logged in user since we create a user on install
* Update the README  with updated installation instructions
* Add gunicorn for running in prod mode

# V0.8
* Standardized config.py so it can be shared without secrets.
* All config options are set in the .env file 
* Fixed volume_path in the build_midpoint_container function to not attempt to assign the variable twice (volume_path = volume_path = something())
* Removed config.py.sample and removed config.py from .gitignore
* Removed unused modules
* Added run_after_start for docker_files model. This will be used to have the option to run a command after the contaienr starts
* Added docker images to git repo. These docker images are used for vulnerable sites
* Added web installer. This will populate connection token and dockerfiles databases. As well as configure the initial admin user.

# V0.7
* Added user profile to display active and finished labs
* Added date_started, difficulty, and date_completed to ActiveLabs model
* Removed custom name on docker creation. This would limit how many of the same containers can be used at a time and cause an exception. Though this doesn't fix the same port being used by multiple containers
* Added lab profile to give more details about labs that are running and completed

# V0.6
* Added destroy network class to mark lab inactive and destroy containers
* Finished lab submission

# V0.5
* Rebuilt how docker containers are spun up and the start lab process
* Catch possible exceptions in the container creation process
* Changed \<b\> tag to \<strong\> in base.html file
* Added config.py.sample and env_sample
* Fixed project name in docker-compose file
* Updated README with install instructions

# V0.4
* Modified ActiveLabs model to store container ID's that are stored in the database as well as the docker id
* Added a running column to ActiveLabs model to still keep lab for historical purposes. (May end up renaming the table but not yet)
* Added a user_id field to ActiveLabs model
* Altered DockerFiles model to store ports as a list in the event multiple ports are needed. A port will need to be stored in the DB like "{'container_port': 'host_port'}" (not double quotes) 
* Altered DockerFiles model to change lab_token_type to lab_token_desc since that's what is stored there
* Changed base template layout to include admin pages
* Added a submit lab page
* Added Users model check to see if a user is an admin
* Completed Admin add Dockerfiles page. The dockerfile still needs to be created on the server first then added
* Completed Admin add connection token page. The connection token still needs to be created on the server first then added.


# V0.3
* Fixed connection token attachment. The connection token is used to connect the container to the one before it. So it should mount as so
* Added seperate midpoint start for samba containers. 4 ports have to be mapped and I didn't see that when I designed the database. This should be fixed at some point. But, since it's probably the only one I'll do it this way for now.
* Added directory for connection tokens
* Added Samba connection token
* Added reload for container variables after creation to refresh attrs
* Fixed how database info size for container ID's 
* Fixed which IP address is being written to the database now getting the SIP_Network IP
* Added name to docker run command since it will be easier to manager containers that way
* Add logout functionality

# V0.2
* Added connection tokens to the dockerfiles model 
* Added connection token volumes to the spin_containers function
* Added check_connection_run function to the BuildNetwork class
* Changed containers to use the correct docker network. 
* Added Samba docker file
* Added webmin exploitable dockerfile (uses CVE-2019-15107) this requires webmin be installed but is not included in this repo.

# V0.1
* Updated DockerFile model 
* Added the active labs model
* Added build network class
* Added web page after the network has been built with lab instructions 