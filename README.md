# NETWORKDECK

NETWORKDECK is CTF and ethical hacking challenge platform. That is aimed towards network security and ethical hacking students and professionals. NETWORKDECK aims to provide a real world CTF style challenge to allow penetration testers to practice their real world skills. Please see https://jgegetskas7.wixsite.com/website for more information.

## Prerequisites
* The setup only works on Linux with a bash shell
* You must have docker and docker-compose installed

## Install
* Clone the repo ```git clone https://gitlab.com/jeffgege/networkdeck.git```
* Run setup.sh ```chmod +x setup.sh && ./setup.sh```
* If you chose not to start the NETWORKDECK container at setup start the container. ```docker-compose up -d --build```
* Access NETWORKDECK by opening a web browser and going to http://YOURIP:5000. Replace YOURIP with the IP address you entered during setup. 
* Once you visit the URL create the admin account used for NETWORKDECK
* Once the admin account is created install setup the database
* Once the database install is complete. You are ready to start hacking. 