#!/bin/bash

# Create .env file
read -r -p "Please enter a new MySQL root password: " mysql_root
read -r -p "Please enter a database name: " mysql_database
read -r -p "Please enter a database username: " mysql_user
read -r -p "Please enterr a password for the database user: " mysql_pass
# Get the IP address. There may be a better way to do this
ip_addr=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')
# Check if that's correct. If not get the right one from user
read -r -p "Is this your local IP address? $ip_addr (Y/N)" correct_ip
if [ "$correct_ip" = "N" ] || [ "$correct_ip" = "n" ]
then
  read -r -p "Please enter your local IP: " ip_addr
fi
echo "Creating .env file"
cwd="$PWD"
# Write the .env file
touch ./.env
echo "ENV_TYPE=prod" >> ./.env
echo "DB_UPGRADE=true" >> ./.env
echo "DB_UPGRADE_MSG=Initial" >> ./.env
echo "MYSQL_ROOT_PASSWORD=$mysql_root" >> ./.env
echo "MYSQL_DATABASE=$mysql_database" >> ./.env
echo "MYSQL_USER=$mysql_user" >> ./.env
echo "MYSQL_PASSWORD=$mysql_pass" >> ./.env
echo "MYSQL_IP=$ip_addr" >> ./.env
echo "DOCKER_CONNECTION_TOKEN_LOCATION=$cwd" >> ./.env
echo ".env file has been created."
echo "Creating Docker images"
# New docker images have to be defined here
docker=("ldap_server" "samba_file_share" "support_site" "site_tester")
# loop through the array and build the images
for i in "${!docker[@]}";
do
  cd "docker_files/${docker[i]}" || exit
  docker build -t "${docker[i]}" .
  cd "$cwd" || exit
  echo "Built docker image ${docker[i]}"
done
echo "All images have been built."
# Check if user wants to start NETWORKDECK now
read -r -p "Do you want to run NETWORKDECK now? (Y/n) " start_now
if [ "$start_now" = "Y" ] || [ "$start_now" = "y" ]
then
  if [ "$cwd" != "$PWD" ]
  then
    cd "$cwd" || exit
  fi
  docker-compose up -d --build
  echo "NETWORKDECK Started. You can access the application at http://$ip_addr:5000"
  exit
fi
# If not tell them which command to use
echo "Please run docker-compose up -d --build to start NETWORKDECK"