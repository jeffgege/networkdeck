<?php
require("require.php");
?>
<h1>Check if a site is up</h1>
<h3>Enter the website you are trying to check below</h3>
<form action="#" method="post">
    <input type="text" class="form-control" name="site" placeholder="www.google.com/8.8.8.8">
    <input type="submit" name="test" value="Test">
</form>

<?php
if(isset($_POST['test'])) {
    $site = $_POST['site'];
    $response = shell_exec('ping -c4 '.$site);
    echo "<div id='response'>". $response. "</div>";
}
?>