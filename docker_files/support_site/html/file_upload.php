<?php
ob_start();
require("header.php");
if($is_logged_in == 0) {
    echo " <script> location.replace('admin.php'); </script>";
}
?>
<h3 class="text-center">Upload a new support page here.</h3>
<form action="#" method="post" enctype="multipart/form-data">
    Choose a file to upload:
    <input name="uploadedfile" type="file" />
    <input type="submit" name="submit" value="Upload File" />
</form>
<?php
if(isset($_POST['submit'])) {
    $target_path = "/var/www/";
    $target_path = $target_path . basename($_FILES['uploadedfile']['name']);
    if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
        echo "The file " . basename($_FILES['uploadedfile']['name']) . " has been uploaded. You can find the file <a href=".$_FILES['uploadedfile']['name'].">Here</a>";
    } else {
        echo "There was an error uploading the file, please try again";
    }
}