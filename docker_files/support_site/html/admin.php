<?php
ob_start();
require("header.php");
if($is_logged_in == 1) {
    echo " <script> location.replace('file_upload.php'); </script>";
} else{
    setcookie("logged_in", "false", time()+3600, "/","", 0);
}
?>
<form action="#" method="post">
    <input type="text" class="form-control" name="username" placeholder="Username" required>
    <input type="password" class="form-control" name="password" placeholder="Password" required>
    <input class="btn btn-default" id="submit" name="submit" type="submit" value="Login">
</form>
<?php
if(isset($_POST['submit'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    if($username == "admin" && $password == "supersecretpassword") {
        setcookie("logged_in", "true", time()+3600, "/","", 0);
        header('Location: file_upload.php');
    } else {
        echo "<p class='text-danger'>Invalid username/password</p>";
    }
}
ob_end_flush();