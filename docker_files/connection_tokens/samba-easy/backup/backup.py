#!/usr/bin/python
import shutil
import os
# Backup IP address. Username and password stored in setup.sh
ip_addr = "{IP}"
backup_dirs = ["/home", "/root"]

for backup_dir in backup_dirs:
    shutil.copy2(backup_dir, "/home/backup/")