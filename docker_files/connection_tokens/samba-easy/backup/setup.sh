#!/bin/bash
echo "Setting up backup script"

destination_ip="$1"
echo "Backup server running on $destination_ip"

echo "Writing crontab file"
echo "00 23 */1 * * root /usr/bin/python /opt/backup.py >> /var/log/backup.log" > /etc/crontab

echo "Moving backup script"
cp /usr/share/backup/backup.py /opt/backup.py

echo "Adding IP to backup script"
sed -i "s/ip_addr = \"{IP}\"/ip_addr = \"$destination_ip\"/" /root/backup.py

mount_username="backupaccount"
mount_password="Password1!"
# This script isn't working right now. For now just connect to the ip using smb Ex. smb://IP_ADDR/files
# mount -t cifs //$destination_ip/files -o username=$mount_username,password=$mount_password,uid=1000,gid=1000 /home/backup

echo "Backup script setup completed"