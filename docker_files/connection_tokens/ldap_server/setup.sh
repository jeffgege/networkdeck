#!/bin/bash

echo "Setting LDAP server IP to $1"
echo "Creating ldap search script in /root/"
mkdir -p /root/
echo "#!/bin/bash" >> /root/ldap_search.sh
echo "if [$# -eq 0 ]" >> /root/ldap_search.sh
echo "then" >> /root/ldap_search.sh
echo "	echo \"You must pass a password as a parameter for the admin user\"" >> /root/ldap_search.sh
echo "	exit 1" >> /root/ldap_search.sh
echo "fi" >> /root/ldap_search.sh
echo "LDAP_IP=$1" >> /root/ldap_search.sh
echo "ldapsearch -x -b dc=fakenetworkdeck,dc=org -D \"cn=admin,dc=fakenetworkdeck,dc=org\" -w \$1" >> /root/ldap_search.sh
