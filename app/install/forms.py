from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, IntegerField, SelectField, FileField, BooleanField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo


class AddUserForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    confirm_password = PasswordField("Confirm Password", validators=[DataRequired(), EqualTo('password')])
    first_name = StringField("First Name", validators=[DataRequired()])
    last_name = StringField("Last Name", validators=[DataRequired()])
    email = StringField("Email")
    submit = SubmitField("Next")


class DBInstallForm(FlaskForm):
    submit = SubmitField("Start DB Install")