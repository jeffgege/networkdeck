from flask import render_template, redirect, url_for, flash, request, session, current_app
from app import db
from flask_login import current_user, login_required
from app.install import bp
from app.install.forms import AddUserForm, DBInstallForm
from app.models import User, DockerFiles, ConnectionTokens
from sqlalchemy.exc import SQLAlchemyError
import docker
import datetime


@bp.route('/index', methods=['GET', 'POST'])
def index():
    form = AddUserForm()
    if form.validate_on_submit():
        try:
            user = User(username=form.username.data, email=form.email.data, first_name=form.first_name.data,
                        last_name=form.last_name.data, date_created=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                        enabled=True, permission=1)
            user.set_password(form.password.data)
            db.session.add(user)
            db.session.commit()
        except SQLAlchemyError:
            current_app.logger.exception("Unable to add user to database")
            flash("Error adding user to database. Please check logs for error message.")
            return redirect(url_for("install.index"))
        return redirect(url_for("install.db_install"))
    return render_template("install/index.html", form=form)


@bp.route('/db_install', methods=['GET', 'POST'])
def db_install():
    form = DBInstallForm()
    if form.validate_on_submit():
        connection_tokens = [
            {'token_destination': None, 'connection_run_command': None, 'friendly_name': 'None', 'token_src': None},
            {'token_destination': '/usr/share/backup',
             'connection_run_command': 'sh /usr/share/backup/setup.sh {IP} &> /var/log/backup_setup.log',
             'friendly_name': 'Samba Backup Script', 'token_src': 'samba-easy/backup/'},
            {'token_destination': '/tmp',
             'connection_run_command': 'sh /tmp/ldap_server/setup.sh {IP} >> /var/log/ldap_install.log',
             'friendly_name': 'LDAP_Search',
             'token_src': 'ldap_server/'}]
        for connection_token in connection_tokens:
            try:
                new_connection_token = ConnectionTokens(friendly_name=connection_token['friendly_name'],
                                                        token_src=connection_token['token_src'],
                                                        token_destination=connection_token['token_destination'],
                                                        connection_run_command=connection_token['connection_run_command'])
                db.session.add(new_connection_token)
                db.session.commit()
            except SQLAlchemyError:
                current_app.logger.exception("Unable to add connection token.")
                flash('Unable to add new connection token. Please check logs for error message.')
                return redirect(url_for("install.index"))
        flash('Successfully added new connection token')
        docker_files = [{'name': 'Samba_Easy', 'entrypoint': False, 'exit_node': True, 'container_name': 'samba_file_share',
                         'port_list': '{"139": "139", "445": "445", "137": "137/udp", "138": "138/udp"}',
                         'container_entrypoint': None, 'lab_difficulty': 'Easy',
                         'lab_token_desc': 'Prod Database Password File',
                         'lab_token': 'prod_sa:C9u#5=*^sCy-MEZ$', 'connection_token': 2,
                         'run_after_start': None},
                        {'name': 'LDAP_Server', 'entrypoint': False, 'exit_node': True, 'container_name': 'ldap_server',
                         'port_list': '{"389": "389", "636": "636"}',
                         'container_entrypoint': None, 'lab_difficulty': 'Easy',
                         'lab_token_desc': 'Find the executives email in the LDAP database.',
                         'lab_token': 'executive_email@fakenetworkdeck.org', 'connection_token': 3,
                         'run_after_start': 'ldapadd -x -D "cn=admin,dc=fakenetworkdeck,dc=org" -w Password1! -f /container/service/slapd/assets/test/executive.ldif'},
                        {'name': 'Support_Site', 'entrypoint': True, 'exit_node': False, 'container_name': 'support_site',
                         'port_list': '{"80": "80"}',
                         'container_entrypoint': None, 'lab_difficulty': 'Easy',
                         'lab_token_desc': None,
                         'lab_token': None, 'connection_token': None,
                         'run_after_start': None},
                        {'name': 'Site_Tester', 'entrypoint': True, 'exit_node': False, 'container_name': 'site_tester',
                         'port_list': '{"80": "80"}', 'container_entrypoint': None, 'lab_difficulty': 'Easy',
                         'lab_token_desc': None, 'lab_token': None, 'connection_token': None,
                         'run_after_start': None}]
        for docker_file in docker_files:
            try:
                new_docker_upload = DockerFiles(name=docker_file['name'], entrypoint=docker_file['entrypoint'],
                                                exit_node=docker_file['exit_node'], container_name=docker_file['container_name'],
                                                port_list=docker_file['port_list'],
                                                container_entrypoint=docker_file['container_entrypoint'],
                                                lab_difficulty=docker_file['lab_difficulty'],
                                                lab_token_desc=docker_file['lab_token_desc'], lab_token=docker_file['lab_token'],
                                                connection_token=docker_file['connection_token'],
                                                run_after_start=docker_file['run_after_start'])
                db.session.add(new_docker_upload)
                db.session.commit()
            except SQLAlchemyError:
                current_app.logger.exception("Unable to add Dockerfile.")
                flash('Unable to add new Dockerfiles. Please check logs for error message.')
                return redirect(url_for("install.index"))
        flash("Dockerfiles have been added.")
        return redirect(url_for('auth.login'))
    return render_template("install/db_install.html", form=form)
