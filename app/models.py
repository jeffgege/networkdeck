import os
from time import time
from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from app import db, login
from datetime import datetime, timedelta
from sqlalchemy.orm import relationship
import base64
import redis
import rq


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    password_hash = db.Column(db.String(120), index=True)
    email = db.Column(db.String(120), index=True, unique=True)
    lab_score = db.Column(db.Integer)
    date_created = db.Column(db.DateTime)
    last_seen = db.Column(db.DateTime)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    permission = db.Column(db.Integer)
    enabled = db.Column(db.Boolean)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_reset_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('UTF-8')

    def is_enabled(self):
        return self.enabled

    def get_permission(self):
        return self.permission

    @staticmethod
    def verify_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'], algorithm=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    def is_admin(self):
        if self.permission == 1:
            return True
        else:
            return False

    def get_users_name(self):
        return self.first_name+" "+self.last_name

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class ConnectionTokens(db.Model):
    # Connection token is used to connect this server to the one before it. This will likely be a file or an application
    # that will give details about this host. For example, a script that connects to a file server for 'backups'. Which
    # will give the user the next servers IP and possibly a username/password or at least a connection to look for
    id = db.Column(db.Integer, primary_key=True)
    # Connection token friendly name
    friendly_name = db.Column(db.String(120))
    # This is where the token is on the app server
    token_src = db.Column(db.String(120))
    # This will be where the token is to be stored on the server
    token_destination = db.Column(db.String(120), index=True)
    # This will be the type of connection token (pdf, doc, python, bash, etc...)
    connection_run_command = db.Column(db.String(120), index=True)


class DockerFiles(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), index=True)
    # Will this file be an entrypoint for users
    entrypoint = db.Column(db.Boolean, default=False)
    # Will this file be an exit node, or where the "token" will be
    exit_node = db.Column(db.Boolean, default=False)
    # Container image name
    container_name = db.Column(db.String(120))
    # This will store a JSON string (which will have to be converted back when used.
    port_list = db.Column(db.String(120))
    # Entrypoint for the container. This will most likely be a file or a command
    container_entrypoint = db.Column(db.String(120))
    # Lab difficulty
    lab_difficulty = db.Column(db.String(50))
    # Lab token description which is what the user is looking for
    lab_token_desc = db.Column(db.String(120))
    # What the token will be to complete the lab.
    lab_token = db.Column(db.String(120), index=True)
    # ID of the connection token used. See ConnectionToken class for more info
    connection_token = db.Column(db.Integer, db.ForeignKey('connection_tokens.id'), index=True)
    # run a command after startup. This will commonly be used for exit containers
    run_after_start = db.Column(db.String(255))


class ActiveLabs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    difficulty = db.Column(db.String(25))
    entrypoint_id = db.Column(db.Integer)
    entrypoint_container_id = db.Column(db.String(64), index=True)
    entrypoint_ip = db.Column(db.String(64), index=True)
    midpoint_id = db.Column(db.Integer)
    midpoint_container_id = db.Column(db.String(64), index=True)
    exit_node_id = db.Column(db.Integer)
    exit_node_container_id = db.Column(db.String(64), index=True)
    lab_token = db.Column(db.String(120), index=True)
    lab_token_type = db.Column(db.String(120), index=True)
    date_started = db.Column(db.DateTime)
    date_completed = db.Column(db.DateTime)
    running = db.Column(db.Boolean, default=True)
