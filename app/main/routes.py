from flask import render_template, redirect, url_for, flash, request, session, current_app
from app import db
from flask_login import current_user, login_required
from app.main.forms import CreateLabForm, SubmitLabForm
from app.main import bp
from app.main.handlers import BuildNetwork, DestroyNetwork
import docker
from app.models import ActiveLabs, DockerFiles


@bp.route('/')
@bp.route('/index')
@login_required
def index():
    form = CreateLabForm()
    return render_template('main/index.html', title='Home', form=form)


@bp.route('/start_lab', methods=['POST'])
@login_required
def start_lab():
    difficulty = request.form.get('lab_difficulty')
    new_lab = BuildNetwork(difficulty)
    lab_id = new_lab.run()
    lab = ActiveLabs.query.filter_by(id=lab_id).first()
    return render_template('main/started_lab.html', title='Lab Started', start_ip=lab.entrypoint_ip,
                           lab_token_type=lab.lab_token_type, lab_id=lab_id)


@bp.route('/submit_lab/<lab_id>', methods=['GET', 'POST'])
@login_required
def submit_lab(lab_id):
    form = SubmitLabForm()
    lab = ActiveLabs.query.filter_by(id=lab_id).first()
    if not lab:
        flash("Lab does not exist")
    if form.validate_on_submit():
        if form.lab_token.data == lab.lab_token:
            completed_lab = DestroyNetwork(lab_id)
            if completed_lab.destroy():
                return render_template('main/submit_lab.html', title='Submit Lab', lab_id=lab_id)
            else:
                flash("An error occurred submitting the lab. Please try again.")
                return render_template('main/submit_lab.html', title='Submit Lab', form=form,
                                       lab_token_type=lab.lab_token_type,
                                       lab_id=lab_id)
        else:
            flash("Invalid Token. Please try again.")
            return render_template('main/submit_lab.html', title='Submit Lab', form=form,
                                   lab_token_type=lab.lab_token_type,
                                   lab_id=lab_id)
    return render_template('main/submit_lab.html', title='Submit Lab', form=form, lab_token_type=lab.lab_token_type,
                           lab_id=lab_id)


@bp.route('/profile', methods=['GET'])
@login_required
def profile():
    active_labs = ActiveLabs.query.filter_by(user_id=current_user.get_id(), running=True).all()
    completed_labs = ActiveLabs.query.filter_by(user_id=current_user.get_id(), running=False).all()
    return render_template('main/profile.html', title='Profile', active_labs=active_labs, completed_labs=completed_labs)


@bp.route('/lab_profile/<lab_id>', methods=['GET'])
@login_required
def lab_profile(lab_id):
    lab = ActiveLabs.query.filter_by(id=lab_id).first()
    entry_container = DockerFiles.query.filter_by(id=lab.entrypoint_id).first()
    exit_container = DockerFiles.query.filter_by(id=lab.exit_node_id).first()
    return render_template('main/lab_profile.html', title='Lab Profile', start_ip=lab.entrypoint_ip,
                           lab_token_type=lab.lab_token_type, lab_id=lab_id, entry_container_name=entry_container.name,
                           exit_container_name=exit_container.name, lab_difficulty=lab.difficulty, running=lab.running,
                           lab_token=lab.lab_token)
