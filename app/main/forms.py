from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, IntegerField, SelectField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.models import User


class CreateLabForm(FlaskForm):
    lab_difficulty = SelectField('Lab Difficulty', choices=['Easy', 'Moderate', 'Expert'], default='Easy',
                                 validators=[DataRequired()])
    submit = SubmitField('Start Lab')


class SubmitLabForm(FlaskForm):
    lab_token = StringField("Lab Token", validators=[DataRequired()])
    submit = SubmitField("Submit Token")