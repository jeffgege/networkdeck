import os
import docker
from flask import current_app
from flask_login import current_user
from app import db
from app.models import DockerFiles, ActiveLabs, ConnectionTokens, User
from sqlalchemy.sql.expression import func
from pathlib import Path
from sqlalchemy import exc
import json


class BuildNetwork:
    def __init__(self, difficulty):
        self.client = docker.from_env()
        self.difficulty = difficulty
        self.has_midpoint = False
        self.entry_container = None
        self.entry_db_id = None
        self.midpoint_container = None
        self.midpoint_db_id = None
        self.exit_container = None
        self.exit_db_id = None

    def run(self):
        # The best way I found this is to setup backwards. This way you can pass the right connection tokens
        # to the container
        # Build exit container
        if self.build_exit_container():
            current_app.logger.debug("Started exit container.")
        else:
            current_app.logger.error("Unable to start exit container")
            return False
        # Build midpoint container
        # if self.build_midpoint_container():
        #     current_app.logger.debug("Started midpoint container")
        # else:
        #     current_app.logger.error("Unable to start midpoint container")
        #     return False
        # Build entry container
        if self.build_entry_container():
            current_app.logger.debug("Started entrypoint container")
        else:
            current_app.logger.error("Unable to start the entrypoint container")
            return False
        # Add the labs to the active labs
        active_lab = self.add_active_lab()
        if active_lab:
            current_app.logger.debug("Active lab entry added")
        else:
            current_app.logger.error("Unable to add active lab entry")
            return False
        # Run the run command for the entrypoint
        if self.run_entry_connection():
            current_app.logger.debug("Ran entrypoint connection")
        else:
            current_app.logger.error("Unable to run entrypoint connection")
            return False
        # Run the run command for the midpoint if it exists
        if self.has_midpoint is True:
            if self.run_midpoint_connection():
                current_app.logger.debug("Ran midpoint connection")
            else:
                current_app.logger.error("Unable to run midpoint connection")
                return False
        return active_lab

    def build_entry_container(self):
        # Get connection tokens
        if self.has_midpoint is True:
            connection_token, connection_dest = self.get_midpoint_connection_token()
        else:
            connection_token, connection_dest = self.get_exit_connection_token()
        # Get a random lab entrypoint from the database
        try:
            entrypoint = DockerFiles.query.filter_by(lab_difficulty=self.difficulty, entrypoint=True).order_by(
                func.rand()).first()
            self.entry_db_id = entrypoint.id
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to get entrypoint container from database")
            return False
        # Build the volume path
        volume_path = os.path.join(current_app.config['DOCKER_VOLUME_PATH'], connection_token)
        current_app.logger.debug(volume_path)
        current_app.logger.debug(connection_dest)
        current_app.logger.debug(current_app.config['DOCKER_VOLUME_PATH'])
        # Build the container
        try:
            self.entry_container = self.client.containers.run(entrypoint.container_name, network="sip_network",
                                                              ports=json.loads(entrypoint.port_list),
                                                              volumes={volume_path: {
                                                                  'bind': connection_dest,
                                                                  'mode': 'ro'}},
                                                              detach=True)
        except docker.errors.DockerException:
            current_app.logger.exception("Unable to create entrypoint docker container")
            return False

        except exc.SQLAlchemyError:
            current_app.logger.exception("There was an error getting an element to create the entry docker container")
            return False
        if entrypoint.run_after_start:
            exec_output = self.entry_container.exec_run(cmd=entrypoint.run_after_start, detach=True, stdout=True, stderr=True)
            current_app.logger.info(
                "Running command: " + entrypoint.run_after_start + " Output: " + str(exec_output))
        return True

    def build_midpoint_container(self):
        # Get connection token info
        connection_token, connection_dest = self.get_exit_connection_token()
        # Set has midpoint to True
        self.has_midpoint = True
        # Get a random midpoint lab from the database
        try:
            mid_point = DockerFiles.query.filter_by(lab_difficulty=self.difficulty, entrypoint=False,
                                                    exit_node=False).order_by(func.rand()).first()
            self.midpoint_db_id = mid_point.id
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to get midpoint container from database")
            return False
        volume_path = os.path.join(current_app.config['DOCKER_VOLUME_PATH'], connection_token)
        # Build the container
        try:
            self.midpoint_container = self.client.containers.run(mid_point.container_name, network="sip_network",
                                                                 ports=json.loads(mid_point.port_list),
                                                                 volumes={volume_path: {
                                                                     'bind': connection_dest,
                                                                     'mode': 'ro'}},
                                                                 detach=True)
        except docker.errors.DockerException:
            current_app.logger.exception("Unable to create midpoint docker container")
            return False
        except exc.SQLAlchemyError:
            current_app.logger.exception("There was an error getting an element to create the midpoint docker container")
            return False
        if mid_point.run_after_start:
            exec_output = self.midpoint_container.exec_run(cmd=mid_point.run_after_start, detach=True, stdout=True, stderr=True)
            current_app.logger.info(
                "Running command: " + mid_point.run_after_start + " Output: " + str(exec_output))
        return True

    def build_exit_container(self):
        try:
            exit_node = DockerFiles.query.filter_by(lab_difficulty=self.difficulty, exit_node=True).order_by(
                func.rand()).first()
            self.exit_db_id = exit_node.id
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to get exit container from database")
            return False
        try:
            self.exit_container = self.client.containers.run(exit_node.container_name, network="sip_network",
                                                             ports=json.loads(exit_node.port_list),
                                                             detach=True)
        except docker.errors.DockerException:
            current_app.logger.exception("Unable to create exit container")
            return False

        except exc.SQLAlchemyError:
            current_app.logger.exception("There was an error getting an element to create the exit docker container")
            return False
        if exit_node.run_after_start:
            exec_output = self.exit_container.exec_run(cmd=exit_node.run_after_start, detach=True, stdout=True, stderr=True)
            current_app.logger.info(
                "Running command: " + exit_node.run_after_start + " Output: " + str(exec_output))
        return True

    def get_midpoint_connection_token(self):
        try:
            midpoint_container = DockerFiles.query.filter_by(id=self.midpoint_db_id).first()
            # Get what midpoint is using for the connection token
            connection_token = ConnectionTokens.query.filter_by(id=midpoint_container.connection_token).first()
            return connection_token.token_src, connection_token.token_destination
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to get midpoint connection token src and dest")
            return False

    def get_exit_connection_token(self):
        try:
            exit_container = DockerFiles.query.filter_by(id=self.exit_db_id).first()
            # Get what midpoint is using for the connection token
            connection_token = ConnectionTokens.query.filter_by(id=exit_container.connection_token).first()
            return connection_token.token_src, connection_token.token_destination
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to get exit connection token src and dest")
            return False

    def run_entry_connection(self):
        try:
            # Get midpoint from the database to get the connection token
            if self.has_midpoint is True:
                container = DockerFiles.query.filter_by(id=self.midpoint_db_id).first()
            else:
                container = DockerFiles.query.filter_by(id=self.exit_db_id).first()
            # Get what midpoint is using for the connection token
            connection_token = ConnectionTokens.query.filter_by(id=container.connection_token).first()
            if connection_token.connection_run_command:
                # Check if the run command needs the IP
                if "{IP}" in connection_token.connection_run_command:
                    if self.has_midpoint is True:
                        run_command = connection_token.connection_run_command.replace("{IP}",
                                                                                      self.midpoint_container.attrs[
                                                                                          'NetworkSettings']['Networks'][
                                                                                          'sip_network']['IPAddress'])
                    else:
                        run_command = connection_token.connection_run_command.replace("{IP}",
                                                                                      self.exit_container.attrs[
                                                                                          'NetworkSettings'][
                                                                                          'Networks'][
                                                                                          'sip_network']['IPAddress'])
                else:
                    run_command = connection_token.connection_run_command
                exec_output = self.entry_container.exec_run(cmd=run_command, detach=True, stdout=True, stderr=True)
                current_app.logger.info(
                    "Running command: " + run_command + " Output: " + str(exec_output))
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to get the midpoint connection token")
            return False
        except docker.errors.DockerException:
            current_app.logger.exception("Unable to execute connection token in entrypoint container")
            return False
        return True

    def run_midpoint_connection(self):
        try:
            # Get midpoint from the database to get the connection token
            exit_container = DockerFiles.query.filter_by(id=self.exit_db_id).first()
            # Get what midpoint is using for the connection token
            connection_token = ConnectionTokens.query.filter_by(id=exit_container.connection_token).first()
            if connection_token.connection_run_command:
                # Check if the run command needs the IP
                if "{IP}" in connection_token.connection_run_command:
                    run_command = connection_token.connection_run_command.replace("{IP}",
                                                                                  self.exit_container.attrs[
                                                                                      'NetworkSettings']['Networks'][
                                                                                      'sip_network']['IPAddress'])
                else:
                    run_command = connection_token.connection_run_command
                exec_output = self.midpoint_container.exec_run(cmd=run_command, detach=True, stdout=True, stderr=True)
                current_app.logger.info(
                    "Running command: " + run_command + " Output: " + str(exec_output))
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to get the exit connection token")
            return False
        except docker.errors.DockerException:
            current_app.logger.exception("Unable to execute connection token in midpoint container")
            return False
        return True

    def add_active_lab(self):
        try:
            # Reload the containers to update the attributes
            self.entry_container.reload()
            self.exit_container.reload()
            # Get the exit node for the lab token
            exit_node = DockerFiles.query.filter_by(id=self.exit_db_id).first()
            # Add database entry
            active_labs = ActiveLabs(user_id=current_user.get_id(),
                                     entrypoint_id=self.entry_db_id,
                                     entrypoint_container_id=self.entry_container.attrs['Id'],
                                     entrypoint_ip=self.entry_container.attrs['NetworkSettings']['Networks']['sip_network']['IPAddress'],
                                     exit_node_id=self.exit_db_id,
                                     exit_node_container_id=self.exit_container.attrs['Id'],
                                     lab_token=exit_node.lab_token,
                                     lab_token_type=exit_node.lab_token_desc,
                                     date_started=current_app.config['TIMESTAMP'],
                                     difficulty=self.difficulty,
                                     running=True
                                     )
            # Check if we are using a midpoint container if so add that as well
            if self.midpoint_container is not None:
                self.midpoint_container.reload()
                active_labs.midpoint_id = self.midpoint_db_id
                active_labs.midpoint_container_id = self.midpoint_container.attrs['Id']
            db.session.add(active_labs)
            db.session.commit()
        # Catch the exceptions
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to add active labs to the database")
            return False
        except docker.errors.DockerException:
            current_app.logger.exception("Unable to refresh or get docker attributes for active lab")
            return False
        db.session.refresh(active_labs)
        return active_labs.id


class DestroyNetwork:
    def __init__(self, active_lab_id):
        self.active_lab_id = active_lab_id
        self.active_lab = ActiveLabs.query.filter_by(id=self.active_lab_id).first()
        self.client = docker.from_env()

    def destroy(self):
        if self.update_db():
            current_app.logger.debug("Updated the active lab database")
        else:
            current_app.logger.error("Unable to update active lab database for "+str(self.active_lab_id))
            return False
        if self.destroy_containers():
            current_app.logger.info("Containers have been destroyed for "+str(self.active_lab_id))
        else:
            current_app.logger.error("Unable to destroy containers for "+str(self.active_lab_id))
            return False
        return True

    def update_db(self):
        try:
            self.active_lab.running = False
            self.active_lab.date_completed = current_app.config['TIMESTAMP']
            db.session.add(self.active_lab)
            db.session.commit()
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unble to set lab as finished")
            return False
        return True

    def destroy_containers(self):
        if self.active_lab.entrypoint_container_id:
            try:
                entry_container = self.client.containers.get(self.active_lab.entrypoint_container_id)
            except docker.errors.DockerException:
                current_app.logger.exception("Unable to get the entrylab container")
                return False
            try:
                entry_container.stop()
                entry_container.remove()
            except docker.errors.DockerException:
                current_app.logger.exception("Unable to stop and remove the entry container")
                return False
        if self.active_lab.midpoint_container_id:
            try:
                midpoint_container = self.client.containers.get(self.active_lab.midpoint_container_id)
            except docker.errors.DockerException:
                current_app.logger.exception("Unable to get the entrylab container")
                return False
            try:
                midpoint_container.stop()
                midpoint_container.remove()
            except docker.errors.DockerException:
                current_app.logger.exception("Unable to stop and remove the entry container")
                return False
        if self.active_lab.exit_node_container_id:
            try:
                exit_container = self.client.containers.get(self.active_lab.exit_node_container_id)
            except docker.errors.DockerException:
                current_app.logger.exception("Unable to get the entrylab container")
                return False
            try:
                exit_container.stop()
                exit_container.remove()
            except docker.errors.DockerException:
                current_app.logger.exception("Unable to stop and remove the entry container")
                return False
        return True
