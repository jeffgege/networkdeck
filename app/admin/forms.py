from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, IntegerField, SelectField, FileField, BooleanField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.models import User


class AddDocker(FlaskForm):
    friendly_name = StringField('Container Friendly Name', validators=[DataRequired()])
    entry_point = BooleanField("Entry Point")
    exit_node = BooleanField("Exit Node")
    container_name = StringField('Docker Image Name', validators=[DataRequired()])
    port_list = StringField('Port List', validators=[DataRequired()])
    container_entrypoint = StringField('Container Entrypoint')
    lab_difficulty = SelectField('Lab Difficulty', choices=['Easy', 'Medium', 'Hard'], default='Easy')
    lab_token_desc = StringField('Lab Token Description')
    lab_token = StringField('Lab Token')
    connection_token = SelectField('Connection Token', coerce=int, default="1")
    run_after_start = StringField('Command to run after container starts')
    submit = SubmitField("Submit")


class AddConnection(FlaskForm):
    friendly_name = StringField('Connection Friendly Name', validators=[DataRequired()])
    token_src = StringField('Connection Token Source', validators=[DataRequired()])
    token_dest = StringField('Connection Token Destination', validators=[DataRequired()])
    connection_run_cmd = StringField('Connection Run Command (Optional)')
    submit = SubmitField('Submit')