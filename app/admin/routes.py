from flask import render_template, redirect, url_for, flash, request, session, current_app
from app import db
from flask_login import current_user, login_required
from app.admin import bp
from app.admin.forms import AddDocker, AddConnection
from app.models import DockerFiles, ConnectionTokens
from sqlalchemy.exc import SQLAlchemyError
import docker

# TODO: Would like to eventually add error checking and making sure everything actually exists


@bp.route('/add_docker', methods=['GET', 'POST'])
@login_required
def add_docker():
    if not current_user.is_admin():
        return redirect('main.index')
    form = AddDocker()
    connection_tokens = db.session.query(ConnectionTokens.id, ConnectionTokens.friendly_name)
    form.connection_token.choices = connection_tokens
    if form.validate_on_submit():
        try:
            new_docker_upload = DockerFiles(name=form.friendly_name.data, entrypoint=form.entry_point.data, exit_node=form.exit_node.data,
                                            container_name=form.container_name.data, port_list=form.port_list.data,
                                            container_entrypoint=form.container_entrypoint.data, lab_difficulty=form.lab_difficulty.data,
                                            lab_token_desc=form.lab_token_desc.data, lab_token=form.lab_token.data,
                                            connection_token=form.connection_token.data, run_after_start=form.run_after_start.data)
            db.session.add(new_docker_upload)
            db.session.commit()
            flash('Successfully added new docker file')
            return redirect(url_for('admin.add_docker'))
        except SQLAlchemyError:
            current_app.logger.exception("Unable to add connection token.")
            flash('Unable to add new docker file. Please try again.')
            return redirect(url_for('admin.add_docker'))
    return render_template("admin/add_docker.html", form=form, title="Add Docker File")


@bp.route('/add_connection', methods=['GET', 'POST'])
@login_required
def add_connection():
    if not current_user.is_admin():
        return redirect('main.index')
    form = AddConnection()
    if form.validate_on_submit():
        current_app.logger.info("Submitted")
        try:
            new_connection_token = ConnectionTokens(friendly_name=form.friendly_name.data, token_src=form.token_src.data,
                                                    token_destination=form.token_dest.data,
                                                    connection_run_command=form.connection_run_cmd.data)
            db.session.add(new_connection_token)
            db.session.commit()
            flash('Successfully added new connection token')
            return redirect(url_for('admin.add_connection'))
        except SQLAlchemyError as e :
            current_app.logger.exception("Unable to add connection token.")
            flash('Unable to add new connection token')
            return redirect(url_for('admin.add_connection'))
    return render_template("admin/add_connection.html", form=form, title="Add Connection Token")
